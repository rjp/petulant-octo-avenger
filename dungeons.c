/* # Dungeon creator */

/* DANCE DANCE THE BOILERPLATE DANCE */
#include <stdio.h>
#include <stdlib.h>

/* Hardcode the height and width for now. */
#define DUNGEON_WIDTH 128
#define DUNGEON_DEPTH 128

/* We need various kinds of tiles for the dungeon. */
#define TILE_NULL 0
#define TILE_ROOM 1
#define TILE_PATH 2
#define TILE_LINK 3

/* Filthy global but we can localise later when things work proper */
int dungeon_tiles[DUNGEON_WIDTH][DUNGEON_DEPTH];

/* How many attempts at placing rooms will we make before giving up? */
#define PLACE_ROOM_ATTEMPTS 50

/* What's the biggest room we'll attempt to make? */
#define ROOM_WIDTH_MAX 12
#define ROOM_DEPTH_MAX 12

/* What's the smallest room we'll attempt to make? */
#define ROOM_WIDTH_MIN 3
#define ROOM_DEPTH_MIN 3

/* A structure for holding the position and size of a room. */
struct room {
	int x, y;
	int w, d;
};

/* We can never have more rooms than place attempts, obviously. */
struct room dungeon_rooms[PLACE_ROOM_ATTEMPTS];

/* Return a random room within the constraints of our dungeon */
struct room
random_room(void)
{
}

int
main (void)
{
	int x, y;

	for (y=0; y<DUNGEON_DEPTH; y++) {
		for (x=0; x<DUNGEON_WIDTH; x++) {
/* `TILE_NULL` means "unassigned", not empty. */
			dungeon_tiles[x][y] = TILE_NULL;
		}
	}
}
